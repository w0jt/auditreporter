# README #

This is a standard Maven project - dependency resolution, running tests etc. is taken care of by the tool and no custom approach is required; tested on Linux and Windows.

The application adheres to the specification. When executed with the "--help" option, it also presents usage information like so:

    usage: reporter [options] users_file files_file
    -c,--csv       print the report in csv instead of plain text
    -h,--help      print this message
    -t,--top <n>   print n-largest files sorted by size

Example: when executed with the command line arguments "-c --top 4 users.csv files.csv" (and provided that these are the standard, unchanged resource files), the output is as follows:

    movie.avi,jpublic,734003200
    pic.jpg,atester,5372274
    audit.xlsx,jpublic,1638232
    holiday.docx,atester,570110
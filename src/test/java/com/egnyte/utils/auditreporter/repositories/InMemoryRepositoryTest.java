package com.egnyte.utils.auditreporter.repositories;

import com.egnyte.utils.auditreporter.entities.UserFile;
import com.egnyte.utils.auditreporter.exceptions.InvalidFileFormatException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InMemoryRepositoryTest {

    private InMemoryRepository repository;

    @Before
    public void setUp() throws Exception {
        repository = new InMemoryRepository();
    }

    @Test
    public void loadFromCorrectFiles() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                "1,jpublic\n" +
                "2,atester"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n" +
                "5974448e-9afd-4c9a-ac5a-9b1e84227820,5372274,pic.jpg,2\n" +
                "fab16fa4-8251-4394-a673-c961a65eb1d2,1638232,audit.xlsx,1\n" +
                "b4f3eecf-95aa-42a7-bffc-83a5441b7d2e,734003200,movie.avi,1\n" +
                "675672f6-a3ff-4872-baa9-955feead534d,570110,holiday.docx,2\n" +
                "73cadd04-c810-4b7d-9516-7b65a22a8cff,150680,marketing.txt,1"
        );

        repository.loadData(usersInputStream, userFilesInputStream);

        assertEquals(2, repository.getAllUsers().size());
        assertEquals(3, repository.getAllUsers().get(0).getFiles().size());
        assertEquals(2, repository.getAllUsers().get(1).getFiles().size());

        assertEquals(0, repository.getFilesSortedBySize(-1).size());
        assertEquals(0, repository.getFilesSortedBySize(0).size());
        assertEquals(3, repository.getFilesSortedBySize(3).size());
        assertEquals(5, repository.getFilesSortedBySize(10).size());

        //check the order
        List<UserFile> filesSortedBySize = repository.getFilesSortedBySize(5);
        assertEquals("movie.avi", filesSortedBySize.get(0).getName());
        assertEquals("pic.jpg", filesSortedBySize.get(1).getName());
        assertEquals("audit.xlsx", filesSortedBySize.get(2).getName());
        assertEquals("holiday.docx", filesSortedBySize.get(3).getName());
        assertEquals("marketing.txt", filesSortedBySize.get(4).getName());
    }

    @Test
    public void loadFromCorrectFilesNoUsers() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n" +
                "5974448e-9afd-4c9a-ac5a-9b1e84227820,5372274,pic.jpg,2\n" +
                "fab16fa4-8251-4394-a673-c961a65eb1d2,1638232,audit.xlsx,1\n" +
                "b4f3eecf-95aa-42a7-bffc-83a5441b7d2e,734003200,movie.avi,1\n" +
                "675672f6-a3ff-4872-baa9-955feead534d,570110,holiday.docx,2\n" +
                "73cadd04-c810-4b7d-9516-7b65a22a8cff,150680,marketing.txt,1"
        );

        repository.loadData(usersInputStream, userFilesInputStream);

        assertEquals(0, repository.getAllUsers().size());

        assertEquals(0, repository.getFilesSortedBySize(-1).size());
        assertEquals(0, repository.getFilesSortedBySize(0).size());
        assertEquals(0, repository.getFilesSortedBySize(1).size());
    }

    @Test
    public void loadFromCorrectFilesNoUserFiles() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                "1,jpublic\n" +
                "2,atester"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n"
        );

        repository.loadData(usersInputStream, userFilesInputStream);

        assertEquals(2, repository.getAllUsers().size());
        assertEquals(0, repository.getAllUsers().get(0).getFiles().size());
        assertEquals(0, repository.getAllUsers().get(1).getFiles().size());

        assertEquals(0, repository.getFilesSortedBySize(-1).size());
        assertEquals(0, repository.getFilesSortedBySize(0).size());
        assertEquals(0, repository.getFilesSortedBySize(1).size());
    }

    @Test(expected = InvalidFileFormatException.class)
    public void invalidUsersFileFormat() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                        "1,,jpublic\n"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n"
        );

        repository.loadData(usersInputStream, userFilesInputStream);
    }

    @Test(expected = InvalidFileFormatException.class)
    public void invalidUserFilesFileFormat() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                        "1,jpublic\n"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n" +
                " ,-5372274,pic.jpg,2\n"
        );

        repository.loadData(usersInputStream, userFilesInputStream);
    }

    @Test(expected = NumberFormatException.class)
    public void incorrectNumericValueUsersFile() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                        "NAN,jpublic\n"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n" +
                "5974448e-9afd-4c9a-ac5a-9b1e84227820,5372274,pic.jpg,2\n"
        );

        repository.loadData(usersInputStream, userFilesInputStream);
    }

    @Test(expected = NumberFormatException.class)
    public void incorrectNumericValueUserFilesFile() throws IOException, InvalidFileFormatException {
        InputStream usersInputStream = toStream(
                "USER_ID,USER_NAME\n" +
                        "1,jpublic\n"
        );
        InputStream userFilesInputStream = toStream(
                "FILE_ID,SIZE,FILE_NAME,OWNER_USER_ID\n" +
                        "5974448e-9afd-4c9a-ac5a-9b1e84227820,NAN,pic.jpg,2\n"
        );

        repository.loadData(usersInputStream, userFilesInputStream);
    }

    private InputStream toStream(String content) {
        return IOUtils.toInputStream(content);
    }
}
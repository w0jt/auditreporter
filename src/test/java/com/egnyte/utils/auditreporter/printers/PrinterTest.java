package com.egnyte.utils.auditreporter.printers;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;

import java.util.ArrayList;
import java.util.List;

public abstract class PrinterTest {

    public abstract void standardModeNoUsers();

    public abstract void standardModeUsersWithoutFiles();

    public abstract void standardModeUsersWithFiles();

    public abstract void topFilesModeWithoutFiles();

    public abstract void topFilesModeWithFiles();

    protected List<User> getUsersWithoutFiles() {
        User jpublic = new User(1, "jpublic", new ArrayList<>());
        User atester = new User(2, "atester", new ArrayList<>());
        List<User> allUsers = new ArrayList<>();
        allUsers.add(jpublic);
        allUsers.add(atester);

        return allUsers;
    }

    protected List<User> getUsersWithFiles() {
        User jpublic = new User(1, "jpublic", new ArrayList<>());
        User atester = new User(2, "atester", new ArrayList<>());

        UserFile file0 = new UserFile("5974448e-9afd-4c9a-ac5a-9b1e84227820", 5372274, "pic.jpg", atester);
        UserFile file1 = new UserFile("fab16fa4-8251-4394-a673-c961a65eb1d2", 1638232, "audit.xlsx", jpublic);
        UserFile file2 = new UserFile("b4f3eecf-95aa-42a7-bffc-83a5441b7d2e", 734003200, "movie.avi", jpublic);
        UserFile file3 = new UserFile("675672f6-a3ff-4872-baa9-955feead534d", 570110, "holiday.docx", atester);
        UserFile file4 = new UserFile("73cadd04-c810-4b7d-9516-7b65a22a8cff", 150680, "marketing.txt", jpublic);

        jpublic.getFiles().add(file1);
        jpublic.getFiles().add(file2);
        jpublic.getFiles().add(file4);
        atester.getFiles().add(file0);
        atester.getFiles().add(file3);

        List<User> allUsers = new ArrayList<>();
        allUsers.add(jpublic);
        allUsers.add(atester);

        return allUsers;
    }

    protected List<UserFile> getTopFiles() {
        User jpublic = new User(1, "jpublic", new ArrayList<>());
        User atester = new User(2, "atester", new ArrayList<>());

        UserFile file0 = new UserFile("5974448e-9afd-4c9a-ac5a-9b1e84227820", 5372274, "pic.jpg", atester);
        UserFile file1 = new UserFile("fab16fa4-8251-4394-a673-c961a65eb1d2", 1638232, "audit.xlsx", jpublic);
        UserFile file2 = new UserFile("b4f3eecf-95aa-42a7-bffc-83a5441b7d2e", 734003200, "movie.avi", jpublic);
        UserFile file3 = new UserFile("675672f6-a3ff-4872-baa9-955feead534d", 570110, "holiday.docx", atester);
        UserFile file4 = new UserFile("73cadd04-c810-4b7d-9516-7b65a22a8cff", 150680, "marketing.txt", jpublic);

        List<UserFile> topFiles = new ArrayList<>();
        topFiles.add(file2);
        topFiles.add(file0);
        topFiles.add(file1);
        topFiles.add(file3);
        topFiles.add(file4);

        return topFiles;
    }
}

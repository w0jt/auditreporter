package com.egnyte.utils.auditreporter.printers;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PlainTextPrinterTest extends PrinterTest{

    private Printer printer = new PlainTextPrinter();
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    @Override
    public void standardModeNoUsers() {
        List<User> emptyUserList = new ArrayList<>();
        printer.printUsersWithFiles(emptyUserList);

        assertEquals("", systemOutRule.getLogWithNormalizedLineSeparator());
    }

    @Test
    @Override
    public void standardModeUsersWithoutFiles() {
        List<User> usersWithNoFiles = getUsersWithoutFiles();
        printer.printUsersWithFiles(usersWithNoFiles);

        assertEquals("", systemOutRule.getLogWithNormalizedLineSeparator());
    }

    @Test
    @Override
    public void standardModeUsersWithFiles() {
        List<User> usersWithFiles = getUsersWithFiles();
        printer.printUsersWithFiles(usersWithFiles);

        String expectedOutput = "Audit Report\n" +
                                "============\n" +
                                "## User: jpublic\n" +
                                "* audit.xlsx ==> 1638232 bytes\n" +
                                "* movie.avi ==> 734003200 bytes\n" +
                                "* marketing.txt ==> 150680 bytes\n" +
                                "## User: atester\n" +
                                "* pic.jpg ==> 5372274 bytes\n" +
                                "* holiday.docx ==> 570110 bytes\n";

        assertEquals(expectedOutput, systemOutRule.getLogWithNormalizedLineSeparator());
    }

    @Test
    @Override
    public void topFilesModeWithoutFiles() {
        List<UserFile> emptyFilesList = new ArrayList<>();
        printer.printTopFiles(emptyFilesList);

        assertEquals("", systemOutRule.getLogWithNormalizedLineSeparator());
    }

    @Test
    @Override
    public void topFilesModeWithFiles() {
        List<UserFile> topFiles = getTopFiles();
        printer.printTopFiles(topFiles);

        String expectedOutput = "Top #5 Report\n" +
                                "============\n" +
                                "* movie.avi ==> jpublic, 734003200 bytes\n" +
                                "* pic.jpg ==> atester, 5372274 bytes\n" +
                                "* audit.xlsx ==> jpublic, 1638232 bytes\n" +
                                "* holiday.docx ==> atester, 570110 bytes\n" +
                                "* marketing.txt ==> jpublic, 150680 bytes\n";

        assertEquals(expectedOutput, systemOutRule.getLogWithNormalizedLineSeparator());
    }
}
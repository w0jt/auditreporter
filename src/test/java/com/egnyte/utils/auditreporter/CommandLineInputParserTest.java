package com.egnyte.utils.auditreporter;

import org.apache.commons.cli.ParseException;
import org.junit.Before;
import org.junit.Test;

public class CommandLineInputParserTest {

    CommandLineInputParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new CommandLineInputParser();
    }

    @Test(expected = ParseException.class)
    public void noArgumentsProvided0() throws ParseException {
        parser.parseCommandLineArgs(convertToArray(""));
    }

    @Test(expected = ParseException.class)
    public void noArgumentsProvided1() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("-c --top 5"));
    }

    @Test(expected = ParseException.class)
    public void onlyOneArgumentProvided0() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("someFile"));
    }

    @Test(expected = ParseException.class)
    public void onlyOneArgumentProvided1() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("--top 5 someFile"));
    }

    @Test
    public void noOptionsSpecified() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("someFile0 someFile1"));
    }

    @Test
    public void someOptionsSpecified() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("-c someFile0 someFile1"));
    }

    @Test
    public void allOptionsSpecified() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("-c --top 10 someFile0 someFile1"));
    }

    @Test(expected = ParseException.class)
    public void noSuchOption() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("--undefined someFile0 someFile1"));
    }

    @Test(expected = NumberFormatException.class)
    public void numberOfTopResultsNotPositive() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("someFile0 someFile1 --top 0"));
    }

    @Test(expected = NumberFormatException.class)
    public void numberOfTopResultsInvalid() throws ParseException {
        parser.parseCommandLineArgs(convertToArray("someFile0 someFile1 --top notANumber"));
    }

    private String[] convertToArray(String line) {
        return line.split(" ");
    }
}
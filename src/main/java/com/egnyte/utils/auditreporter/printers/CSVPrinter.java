package com.egnyte.utils.auditreporter.printers;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;

import java.util.List;

public class CSVPrinter implements Printer {
    @Override
    public void printUsersWithFiles(List<User> users) {
        for (User user : users)
            for (UserFile file : user.getFiles())
                System.out.println(user.getName() + "," + file.getName() + "," + file.getSize());
    }

    @Override
    public void printTopFiles(List<UserFile> topFiles) {
        for (UserFile file : topFiles)
            System.out.println(file.getName() + "," + file.getUser().getName() + "," + file.getSize());
    }
}

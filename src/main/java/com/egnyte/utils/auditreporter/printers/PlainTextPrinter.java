package com.egnyte.utils.auditreporter.printers;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;

import java.util.List;

public class PlainTextPrinter implements Printer {
    @Override
    public void printUsersWithFiles(List<User> users) {
        StringBuilder output = new StringBuilder();
        for (User user : users) {
            if (user.getFiles().size() > 0) {
                output.append("## User: " + user.getName() + "\n");
                for (UserFile file : user.getFiles()) {
                    output.append("* " + file.getName() + " ==> " + file.getSize() + " bytes\n");
                }
            }
        }
        if (output.length() > 0) {
            System.out.println("Audit Report");
            System.out.println("============");
            System.out.print(output.toString());
        }
    }

    @Override
    public void printTopFiles(List<UserFile> topFiles) {
        StringBuilder output = new StringBuilder();
        for (UserFile file : topFiles) {
            output.append("* " + file.getName() + " ==> " + file.getUser().getName() + ", " + file.getSize() + " bytes\n");
        }
        if (output.length() > 0) {
            System.out.println("Top #" + topFiles.size() + " Report");
            System.out.println("============");
            System.out.print(output.toString());
        }
    }
}

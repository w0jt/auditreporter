package com.egnyte.utils.auditreporter.printers;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;

import java.util.List;

public interface Printer {

    void printUsersWithFiles(List<User> users);

    void printTopFiles(List<UserFile> topFiles);
}

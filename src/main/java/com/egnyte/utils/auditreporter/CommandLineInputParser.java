package com.egnyte.utils.auditreporter;

import org.apache.commons.cli.*;

import java.util.List;

public class CommandLineInputParser {

    private Options commandLineOptions;

    private String usersFilename;
    private String filesFilename;

    private boolean helpRequested = false;
    private boolean csvOutputEnabled = false;
    private boolean topFilesModeEnabled = false;
    private int numberOfTopResults;

    public CommandLineInputParser() {
        commandLineOptions = new Options();
        Option helpOption = new Option("h", "help", false, "print this message");
        Option csvOption = new Option("c", "csv", false, "print the report in csv instead of plain text");
        Option topOption = OptionBuilder
                .withArgName("n")
                .hasArg()
                .withDescription("print n-largest files sorted by size")
                .withLongOpt("top")
                .create("t");
        commandLineOptions.addOption(helpOption);
        commandLineOptions.addOption(csvOption);
        commandLineOptions.addOption(topOption);
    }

    public void parseCommandLineArgs(String[] args) throws ParseException {

        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(commandLineOptions, args);
        List<String> argList = line.getArgList();

        if (line.hasOption("help"))
            helpRequested = true;
        else {
            if (argList.size() < 2)
                throw new ParseException("Too few arguments provided");

            usersFilename = argList.get(0);
            filesFilename = argList.get(1);

            if (line.hasOption("csv"))
                csvOutputEnabled = true;

            if (line.hasOption("top")) {
                topFilesModeEnabled = true;
                numberOfTopResults = Integer.parseInt(line.getOptionValue("top"));
                if (numberOfTopResults <= 0)
                    throw new NumberFormatException("The number of top documents should be positive");
            }
        }
    }

    public void printUsageInfo() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("reporter [options] users_file files_file", commandLineOptions);
    }

    public String getUsersFilename() {
        return usersFilename;
    }

    public String getFilesFilename() {
        return filesFilename;
    }

    public boolean isHelpRequested() {
        return helpRequested;
    }

    public boolean isCsvOutputEnabled() {
        return csvOutputEnabled;
    }

    public boolean isTopFilesModeEnabled() {
        return topFilesModeEnabled;
    }

    public int getNumberOfTopResults() {
        return numberOfTopResults;
    }
}

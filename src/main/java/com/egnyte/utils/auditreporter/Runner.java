package com.egnyte.utils.auditreporter;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;
import com.egnyte.utils.auditreporter.exceptions.InvalidFileFormatException;
import com.egnyte.utils.auditreporter.printers.CSVPrinter;
import com.egnyte.utils.auditreporter.printers.PlainTextPrinter;
import com.egnyte.utils.auditreporter.printers.Printer;
import com.egnyte.utils.auditreporter.repositories.InMemoryRepository;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Runner {

    public static void main(String[] args) throws IOException {
        CommandLineInputParser parser = new CommandLineInputParser();
        try {
            parser.parseCommandLineArgs(args);

        } catch (ParseException|NumberFormatException e) {
            System.err.println("Could not parse the command line input: " + e.getMessage());
            System.exit(-1);
        }

        if (parser.isHelpRequested()) {
            parser.printUsageInfo();
        }
        else {
            InMemoryRepository repository = new InMemoryRepository();
            try {
                ClassLoader cl = repository.getClass().getClassLoader();

                InputStream usersInputStream = cl.getResourceAsStream(parser.getUsersFilename());
                if (usersInputStream == null)
                    throw new IOException("Could not read file " + parser.getUsersFilename());

                InputStream userFilesInputStream = cl.getResourceAsStream(parser.getFilesFilename());
                if (userFilesInputStream == null)
                    throw new IOException("Could not read file " + parser.getFilesFilename());

                repository.loadData(usersInputStream, userFilesInputStream);

                Printer printer;
                if (parser.isCsvOutputEnabled())
                    printer = new CSVPrinter();
                else
                    printer = new PlainTextPrinter();

                if (parser.isTopFilesModeEnabled()) {
                    List<UserFile> topFiles = repository.getFilesSortedBySize(parser.getNumberOfTopResults());
                    printer.printTopFiles(topFiles);
                }
                else {
                    List<User> allUsers = repository.getAllUsers();
                    printer.printUsersWithFiles(allUsers);
                }
            }
            catch (NumberFormatException e) {
                System.err.println("Could not parse one of the numeric values in the input file: " + e.getMessage());
            }
            catch (InvalidFileFormatException e) {
                System.err.println("Invalid format of the input file: " + e.getMessage());
            }
            catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}

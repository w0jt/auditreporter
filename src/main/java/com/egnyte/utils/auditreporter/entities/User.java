package com.egnyte.utils.auditreporter.entities;

import java.util.List;

public class User {

    private long id;
    private String name;
    private List<UserFile> files;

    public User(long id, String name, List<UserFile> files) {
        this.id = id;
        this.name = name;
        this.files = files;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserFile> getFiles() {
        return files;
    }

    public void setFiles(List<UserFile> files) {
        this.files = files;
    }
}

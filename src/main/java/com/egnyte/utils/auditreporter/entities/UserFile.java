package com.egnyte.utils.auditreporter.entities;

public class UserFile {

    private String id;
    private long size;
    private String name;
    private User user;

    public UserFile(String id, long size, String name, User user) {
        this.id = id;
        this.size = size;
        this.name = name;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

package com.egnyte.utils.auditreporter.repositories;

import com.egnyte.utils.auditreporter.entities.User;
import com.egnyte.utils.auditreporter.entities.UserFile;
import com.egnyte.utils.auditreporter.exceptions.InvalidFileFormatException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class InMemoryRepository {

    private Map<Long, User> users = new HashMap<>();
    private List<UserFile> filesSortedBySize = new ArrayList<>();

    public void loadData(InputStream usersInputStream, InputStream userFilesInputStream) throws IOException, InvalidFileFormatException {
        //prepare the structures
        users.clear();
        filesSortedBySize.clear();

        BufferedReader reader = new BufferedReader(new InputStreamReader(usersInputStream));

        String line;
        try {
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                List<String> userRow = Arrays.asList(line.split(","));
                if (userRow.size() < 2)
                    throw new InvalidFileFormatException("Too few fields in the input file; line: " + line);

                long userId = Long.parseLong(userRow.get(0));
                String userName = userRow.get(1);
                if (userName.trim().isEmpty())
                    throw new InvalidFileFormatException("Username cannot be empty; line: " + line);

                users.put(userId, new User(userId, userName, new ArrayList<UserFile>()));
            }
        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }

        reader = new BufferedReader(new InputStreamReader(userFilesInputStream));
        try {
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                List<String> filesRow = Arrays.asList(line.split(","));
                if (filesRow.size() < 4)
                    throw new InvalidFileFormatException("Too few fields in the input file; line: " + line);

                String fileId = filesRow.get(0);
                if (fileId.trim().isEmpty())
                    throw new InvalidFileFormatException("File id cannot be empty; line: " + line);

                long fileSize = Long.parseLong(filesRow.get(1));
                if (fileSize <= 0)
                    throw new InvalidFileFormatException("File size must be a positive number; line: " + line);

                String fileName = filesRow.get(2);
                if (fileName.trim().isEmpty())
                    throw new InvalidFileFormatException("File name cannot be empty; line: " + line);

                long userId = Long.parseLong(filesRow.get(3));
                User owner = users.get(userId);

                if (owner != null) {
                    UserFile file = new UserFile(fileId, fileSize, fileName, owner);
                    filesSortedBySize.add(file);
                    users.get(userId).getFiles().add(file);
                }
            }
        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }

        filesSortedBySize.sort((fileA, fileB) -> (fileA.getSize() > fileB.getSize()) ? -1 : 1);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(users.values());
    }

    public List<UserFile> getFilesSortedBySize(int numberOfFiles) {
        int n = Math.min(filesSortedBySize.size(), numberOfFiles);
        n = Math.max(n, 0);
        return filesSortedBySize.subList(0, n);
    }
}
